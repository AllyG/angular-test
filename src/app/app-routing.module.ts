import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CanvasComponent} from './canvas/canvas.component';
import {CalculatorComponent} from './calc/calculator/calculator.component';
import {TestImageComponent} from './test-image/test-image.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'canvas'},
  {path: 'canvas', component: CanvasComponent},
  {path: 'calculator', component: CalculatorComponent},
  {path: 'testImage', component: TestImageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
