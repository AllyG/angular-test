import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { BehaviorSubject} from 'rxjs';
import { NumberModel} from '../models/number.model';
import { OperationModel} from '../models/operation.model';
import { PrimaryModel} from '../models/primary.model';

@Injectable({
  providedIn: 'root'
})
export class ServiceCalculatorService {
  public myInput: Array <NumberModel> = [];
  private item = new BehaviorSubject<number>( 35);
  cast = this.item.asObservable();
  addNumber(number) {
    this.myInput.push(number);
    this.item.next(5);
  }
  addOperation(operation) {
    this.myInput.push(operation);
    this.item.next(5);
  }
  addPrimary(primary) {
    this.myInput.push(primary);
    this.item.next(5);
  }

  constructor(public http: HttpClient) {
  }
  /************************************************/
  getDataNumners() {
    return this.http.get('http://localhost:3000/numbers');
  }
  getDataOperations() {
    return this.http.get('http://localhost:3000/operation');
  }
  getDataPrimary() {
    return this.http.get('http://localhost:3000/primary');
  }
}
