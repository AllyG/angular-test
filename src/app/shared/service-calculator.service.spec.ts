import { TestBed, inject } from '@angular/core/testing';

import { ServiceCalculatorService } from './service-calculator.service';

describe('ServiceCalculatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServiceCalculatorService]
    });
  });

  it('should be created', inject([ServiceCalculatorService], (service: ServiceCalculatorService) => {
    expect(service).toBeTruthy();
  }));
});
