import {Component, OnInit, Input, AfterViewInit, ElementRef, ViewChild, ContentChild} from '@angular/core';

import {NumberModel} from '../../models/number.model';
import { ServiceCalculatorService} from '../../shared/service-calculator.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})

export class InputComponent implements OnInit {
  public myInput: Array <NumberModel> = [];
  constructor(private service: ServiceCalculatorService) {
  }

  ngOnInit() {
    this.service.cast.subscribe((numbers ) => {
      this.sincInput();
      // this.numberValue;
    });
  }
  // ngAfterViewInit () {
  // }
  sincInput() {
    this.myInput = this.service.myInput;
    console.log(this.myInput);
  }
}
