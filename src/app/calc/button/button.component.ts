import { Component, OnInit } from '@angular/core';
import {NumberModel} from '../../models/number.model';
import {OperationModel} from '../../models/operation.model';
import {PrimaryModel} from '../../models/primary.model';
import {ServiceCalculatorService} from '../../shared/service-calculator.service';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  public numbers: Array <NumberModel> = [];
  public operations: Array <OperationModel> = [];
  public primary: Array <PrimaryModel> = [];

  constructor(private service: ServiceCalculatorService) { }

  ngOnInit() {
    this.service.getDataNumners().subscribe((data: any ) => {
      this.numbers = data;
    });
    this.service.getDataOperations().subscribe((data: any ) => {
      this.operations = data;
    });
    this.service.getDataPrimary().subscribe((data: any ) => {
      this.primary = data;
    });
  }
  addNumber(number: NumberModel) {
      this.service.addNumber(number);
  }
  addOperation(operation: OperationModel) {
      this.service.addOperation(operation);
  }
  addPrimary(primary: PrimaryModel) {
      this.service.addPrimary(primary);
  }
}
