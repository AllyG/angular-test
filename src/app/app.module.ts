import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {ServiceCalculatorService} from './shared/service-calculator.service';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CanvasComponent } from './canvas/canvas.component';
import { CalculatorComponent } from './calc/calculator/calculator.component';
import { InputComponent } from './calc/input/input.component';
import { ButtonComponent } from './calc/button/button.component';
import { TestImageComponent } from './test-image/test-image.component';


@NgModule({
  declarations: [
    AppComponent,
    CanvasComponent,
    CalculatorComponent,
    InputComponent,
    ButtonComponent,
    TestImageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [ServiceCalculatorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
