import {AfterViewInit, Component, ElementRef, EventEmitter, OnInit, Output, Input, ViewChild} from '@angular/core';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements  AfterViewInit {
  @ViewChild ('myCanvas') myCanvas: ElementRef;

  @Input() public width = 600;
  @Input() public height = 400;

  public cx: CanvasRenderingContext2D;
  public x = 200;
  public y = 100;
  public dx = 20;
  public myJSON;
  public timer;
  public lastElm;
  vector: Array<object> = [];
  data: Array<number> = [];
  // data: any;
  constructor() {
  }
  ngAfterViewInit () {
    const canvasEl: HTMLCanvasElement = this.myCanvas.nativeElement;
    this.cx = canvasEl.getContext('2d');
    /*------------------setare dimensiuni------------------------------*/
    canvasEl.width = this.width;
    canvasEl.height = this.height;
    //
      if ('key' in localStorage) {
        this.refresh();
      } else {
          this.draw();
      }
    //
  }
  /*-----------------deseneaza patrat-----------------*/
  draw () {
    const ctx = this.cx;
    ctx.clearRect(0, 0, 600, 400);
    ctx.beginPath();
    ctx.save();
    ctx.rect(this.x, this.y, 50, 50);
    ctx.strokeStyle = 'red';
    ctx.stroke();
    ctx.restore();
  }
  /*--------------------capturarea ev btn---------------------*/
  moveTop (canvasEl: HTMLInputElement) {
    this.y -= this.dx;
    this.draw();
  }
  moveBottom (canvasEl: HTMLInputElement) {
    this.y += this.dx;
    this.draw();
  }
  moveLeft (canvasEl: HTMLInputElement) {
    this.x -= this.dx;
    this.draw();
  }
  moveRight (canvasEl: HTMLInputElement) {
    this.x += this.dx;
    this.draw();
  }
  rec() {
    this.vector.push({
      ox: this.x,
      oy: this.y
    });
    console.log(this.vector);
      this.myJSON = JSON.stringify(this.vector);
      console.log(this.myJSON);
      localStorage.setItem('key', this.myJSON);
  }
  play() {}
  refresh() {
    this.data = JSON.parse(localStorage.getItem('key'));
    // localStorage.clear();
    if (this.data.length > 0) {
      this.lastElm = this.data.pop();
      this.x = this.lastElm.ox;
      this.y = this.lastElm.oy;
      this.draw();
    } else {
      this.draw();
    }
  }
}
