import {AfterViewInit, ViewChild, Input, ElementRef, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-test-image',
  templateUrl: './test-image.component.html',
  styleUrls: ['./test-image.component.scss']
})
export class TestImageComponent implements OnInit, AfterViewInit {

    @ViewChild ('myCanvas') myCanvas: ElementRef;

    @Input() public width = 600;
    @Input() public height = 400;

    public x = 200;
    public y = 100;
    public cx: CanvasRenderingContext2D;
    public myJSON;
    vector: Array<object> = [];
    image = 'assets/images/test.png';
    constructor() {
    }
    ngAfterViewInit () {
        const canvasEl: HTMLCanvasElement = this.myCanvas.nativeElement;
        this.cx = canvasEl.getContext('2d');
        /*------------------setare dimensiuni------------------------------*/
        canvasEl.width = this.width;
        canvasEl.height = this.height;
        //
        // if ('key' in localStorage) {
        //     this.refresh();
        // } else {
        //     this.draw();
        // }
        this.draw();
        //
    }
    /*-----------------deseneaza patrat-----------------*/
    draw () {
        const ctx = this.cx;
        const canvas = this.myCanvas.nativeElement;
        ctx.clearRect(0, 0, 600, 400);
        ctx.beginPath();
        const source = new Image();
        source.crossOrigin = 'Anonymous';
        source.onload = () => {
            ctx.drawImage(source, this.x, this.y, 80, 80);
            this.image = canvas.toDataURL();
            this.vector.push({
                bimg: this.image
            });
            console.log(this.vector);
            this.myJSON = JSON.stringify(this.vector);
            console.log(this.myJSON);
            localStorage.setItem('key', this.myJSON);
        };
        source.src = this.image;
    }

}
